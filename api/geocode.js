"use strict";

const GEOCODE_URL = "https://maps.googleapis.com/maps/api/geocode/json?address=";

const getLocation = function getLocation(address) {
  const request = require('request');
  return new Promise(function(resolve, reject) {
    const encodedAddress = encodeURIComponent(address);  // url stringi olarak kullanmak için değiştir
    const decodedAddress = decodeURIComponent(encodedAddress); // url stringine dönüştürülmüş metni normal metne çevir.

    const requestParams = {
      url : `${GEOCODE_URL}${encodedAddress}`,
      json : true // automatically convert the returned json string to object...
    }

    request(requestParams, function requestCallback(error, response, body) {
      if (error){
          reject("Sunucuya erişilemiyor!"); // alttaki callback ifadesi yerine bu daha iyi...
          //callback("Sunucuya erişilemiyor!"); //sadece error nesnesi ile çağır.
      } else if (body.status == "ZERO_RESULTS") {
          reject("Adres bulunamadı.");
      } else if (response.statusCode == 200) {
          if (body.status == "OK") {
              resolve( {
                  address : body.results[0].formatted_address,
                  lng : body.results[0].geometry.location.lng,
                  lat : body.results[0].geometry.location.lat,
                  placeId : body.results[0].place_id
              });
          } else {
            reject("body.status nok!");
          }
      } else if (response.statusCode == 404){
          reject("Aradığınız sayfa bulunamadı.");
      } else {
          reject("Beklenmeyen hata : " + response.statusCode);
      }

    });

  });
}

module.exports = {
  getLocation
}

// https://api.darksky.net/forecast/14059f23f8e59da642bf792c94318720/37.8267,-122.4233?units=si
