"use strict";

const request = require('request');

const getTemperature = function getTemperature(lat, lng, callback) {
  request({
    url : `https://api.darksky.net/forecast/14059f23f8e59da642bf792c94318720/${lat},${lng}?units=si`,
    json : true
  }, function(err, response, body) {
    if (err){
      callback(err)
    } else {
      if (response.statusCode == 200){
        callback(undefined, body.currently.temperature);
      } else {
        console.log(JSON.stringify(response, undefined, 2));
        callback({
          statusCode : response.statusCode,
          status : "NOK"
        });
      }

    }
  });
}

module.exports = {
  getTemperature
}
