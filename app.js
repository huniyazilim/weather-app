"use strict";

const yargs = require("yargs");
const geocode = require("./api/geocode");
const forecast = require("./api/forecast");
const greeting = require('./greeting');
const fs = require('fs');

const argv = yargs
    .options({
        a : {
            alias : "address",
            string : true,
            describe : "Hava durumu için adres giriniz"
        },
        f : {
          alias : "file",
          string : "true",
          describe : "Geolocation bilgilerini almak istediğiniz yer bilgilerini içeren dosya"
        }
    })
    .help()
    .alias("help", "h")
    .argv;

// greeting.turkish();
// greeting.spanish();
// greeting.english();

if (argv.address) {
  geocode.getLocation(argv.address)
    .then (function onResolve(locationData) {
      console.log(JSON.stringify(locationData, undefined, 2));
      // örnek olması açısından burada callback mantığı ile çalışan fonksiyonu değiştirmedim.
      // forecast.getTemperature(locationData.lat, locationData.lng, function getTemperatureCallback(err, temperature) {
      //   if (err) {
      //     console.log("Bir hata oluştu : ", err);
      //     return;
      //   }
      //   console.log(`Temperature : ${temperature}`);
      // });
      return;
    }, function onReject(errorMessage) {
      console.log(err);
    });
}

const cities = fs.readFileSync("cities.txt", "utf-8");

let geoData = [];

let cityArr = cities.split("|");
console.log(cityArr);

const FindLocations = function FindLocations(arr) {

    if (arr.length>0) {
      console.log("arr.length : " + arr.length );
      const city = arr[0];
      setTimeout(()=>{
        console.log(city);
        geocode.getLocation(city)
        .then((data) => {
          console.log(data);
          geoData.push(data);
          fs.writeFileSync("geoData.txt", JSON.stringify(geoData, null, '\t'), "utf-8");
          const dummy = arr.shift();
          FindLocations(arr);
        }, (err) => {
          console.log(err);
          FindLocations(arr);
        })
        .catch((err)=>{
          console.log(err);
          FindLocations(arr);
        });
      }, 3000);

    } // if

} // FindLocations

console.log("=================================");

FindLocations(cityArr, true);
