"use strict";

const english = require('./english');
const spanish = require('./spanish');

function sayHello() {
  console.log("Merhaba dünya!");
}

module.exports = {
  english,
  spanish,
  turkish : sayHello
}
